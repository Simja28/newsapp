import { Login } from './../interface/login';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private _http: HttpClient) { }

  getUsers(datos: Login) {
    return this._http.get('https://jsonplaceholder.typicode.com/users' + '?username=' + datos.username);
  }
}
