import { Storage } from '@ionic/storage-angular';
import { StorageService } from './../../services/storage.service';
import { Router } from '@angular/router';
import { LoginService } from './../../services/login.service';
import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Login } from 'src/app/interface/login';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: 'login.page.html',
  styleUrls: ['login.page.scss'],
})
export class LoginPage {
  auth: Login = new Login();
  private person: Login[] = [];

  constructor(
    private loginService: LoginService,
    private router: Router,
    private storageService: StorageService,
    public toastController: ToastController
  ) {}

  login(form: NgForm) {
    this.loginService.getUsers(this.auth).subscribe((response: any) => {
      this.person = response;
      console.log(this.person);
      if (this.person.length > 0) {
        this.storageService.saveUser(this.person);
        this.router.navigateByUrl('/home');
      } else {
        console.log('Login incorrecto');
      }
    });
  }
}
